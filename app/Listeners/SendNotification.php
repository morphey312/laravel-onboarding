<?php

namespace App\Listeners;

use App\Events\UpdateTask;
use App\Jobs\SendUpdatedTaskToEmailJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendNotification
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(UpdateTask $event): void
    {
        $task = $event->getModel();
        SendUpdatedTaskToEmailJob::dispatch($task);
    }
}
