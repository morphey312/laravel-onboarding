<?php

namespace App\Jobs;

use App\Contracts\MessengerInterface;
use App\Mail\TaskCreated;
use App\Mail\TaskDeleted;
use App\Models\Task;
use App\Services\Messenger\Messenger;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendNotificationForRemovedTaskJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    /**
     * Create a new job instance.
     */
    public function __construct(
        public Task $task,
        public Collection $users,
    ) {}

    /**
     * Execute the job.
     */
    public function handle(MessengerInterface $messenger): void
    {
        foreach ($this->users as $user) {
            $messenger->send($user, $this->task);
        }
    }
}
