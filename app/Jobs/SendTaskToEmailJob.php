<?php

namespace App\Jobs;

use App\Contracts\MessengerInterface;
use App\Events\TaskCreatedBroadcast;
use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendTaskToEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    /**
     * Create a new job instance.
     */
    public function __construct(
        public Task $task
    ) {}

    /**
     * Execute the job.
     */
    public function handle(MessengerInterface $messenger): void
    {
        broadcast(new TaskCreatedBroadcast($this->task));
        foreach ($this->task->users as $user) {
            $messenger->send($user, $this->task);
        }
    }
}
