<?php

namespace App\Services\Messenger;

use App\Contracts\MessengerInterface;
use App\Models\Task;
use App\Models\User;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class Messenger implements MessengerInterface
{
    public function __construct(
        protected string $purpose
    ) {}
    public function send(User $recipient, Task $task): void
    {
        $mailable = app()->makeWith(
            $this->purpose,
            ['task' => $task]
        );
        Mail::to($recipient->email, $recipient->name)->send($mailable);
    }
}
