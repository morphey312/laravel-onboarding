<?php

namespace App\Events;

use App\Models\Task;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TaskCreatedBroadcast implements ShouldBroadcast
{
    protected Task $task;
    /**
     * Create a new event instance.
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }
    /**
     * Broadcast condition
     *
     * @return bool
     */
    public function broadcastWhen()
    {
        return $this->task->id !== null;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array
    {
        return [
            new Channel('notifications'),
        ];
    }

    public function broadcastWith()
    {
        return [
            'message' => 'Created Task',
            'task_id' => $this->task->id,
            'deadline' => $this->task->users->first()->pivot->deadline,
        ];
    }

    public function broadcastAs()
    {
        return 'task-created';
    }
}
