<?php

namespace App\Http\Controllers;

use App\Events\UpdateTask;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Jobs\SendTaskToEmailJob;
use App\Mail\TaskCreated;
use App\Mail\TaskUpdated;
use App\Models\Category;
use App\Models\Task;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;
use Illuminate\Support\Facades\Gate;

class TaskController extends Controller
{

    public function __construct()
    {
        $this->middleware('check.user');
    }
    public function index(): View
    {
//        $this->authorize('viewAny', Task::class);

        $tasks = Task::query()->latest()->get();

        return view('tasks.index', ['tasks' => $tasks]);
    }

    public function create(): View
    {
        $categories = Category::all();
        $users = User::all();

        return view('tasks.create', compact('categories', 'users'));
    }

    /**
     * @param StoreTaskRequest $request
     * @return RedirectResponse
     */
    public function store(StoreTaskRequest $request): \Illuminate\Http\RedirectResponse
    {
        $validated = $request->validated();
        $taskData = Arr::except($validated, ['users', 'deadline']);
        if ($request->file('image') !== null) {
            $taskData['image'] = $request->file('image')->store('images');
        }
        $usersData = Arr::get($validated, 'users', []);
        $deadline = Arr::get($validated, 'deadline', null);
        $task = Task::create($taskData);
        $task->users()->attach($usersData, ['deadline' => $deadline]);

        SendTaskToEmailJob::dispatch($task, $request->user());

        return redirect()->route('tasks.index');
    }

    public function show(Task $task): View
    {
        $user = request()->user();
        if ($user && $user->can('view', $task)) {
            return view('tasks.show', ['task' => $task]);
        }
        abort(403);

//        $this->authorize('view', $task);
//
//        return view('tasks.show', ['task' => $task]);

    }

    public function edit(Task $task)
    {
        Gate::authorize('update-task', $task);
        $categories = Category::all();
        $users = User::all();
        $selectedUsers = $task->users()->get(['id'])->pluck('id')->toArray();
        $date = $task->users()->first()->pivot->deadline;
        $deadline = Carbon::parse($date)->format('Y-m-d');

        return view('tasks.edit', compact('task', 'categories', 'users', 'selectedUsers', 'deadline'));
    }

    public function update(UpdateTaskRequest $request, Task $task)
    {
        $validated = $request->validated();
        $taskData = Arr::except($validated, ['users', 'deadline']);
        $users = Arr::get($validated, 'users', []);
        $deadline = Arr::get($validated, 'deadline', null);
        $task->update($validated);
        $task->users()->syncWithPivotValues($users, ['deadline' => $deadline]);
        event(new UpdateTask($task));

        return redirect()->route('tasks.index');
    }

    public function destroy(Task $task)
    {
        $task->delete();
        return redirect()->route('tasks.index');
    }
}
