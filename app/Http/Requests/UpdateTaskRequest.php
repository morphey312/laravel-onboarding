<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'nullable|string|min:3|max:100',
            'description' => 'sometimes|required|string|min:10|max:1000',
            'category_id' => 'bail|required|integer|exists:App\Models\Category,id',
            'users' => 'required|array',
            'users.*' => 'required|integer|exists:App\Models\User,id',
            'deadline' => 'nullable|date|after_or_equal:today',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'name.required' => 'Ім\'я є обов\'язковим',
            'name.min' => 'Ім\'я повинно бути не менше 3 символів',
            'description.required' => 'Опис є обов\'язковим',
            'category_id.required' => 'Категорія є обов\'язковою',
            'users.exists' => 'Користувача з таким id не існує',
            'deadline.after_or_equal' => 'Дата не може бути в минулому'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'category_id' => 'категорія',
            'users' => 'користувачі',
            'deadline' => 'строк завершення',
        ];
    }
}
