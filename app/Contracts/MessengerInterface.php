<?php

namespace App\Contracts;


use App\Models\Task;
use App\Models\User;

interface MessengerInterface
{
    public function send(User $recipient, Task $task): void;
}
