<?php

namespace App\Providers;

use App\Contracts\MessengerInterface;
use App\Jobs\SendNotificationForRemovedTaskJob;
use App\Jobs\SendTaskToEmailJob;
use App\Jobs\SendUpdatedTaskToEmailJob;
use App\Mail\TaskCreated;
use App\Mail\TaskDeleted;
use App\Mail\TaskUpdated;
use App\Services\Messenger\Messenger;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(
            'mail.created-task',
            TaskCreated::class
        );
        $this->app->bind(
            'mail.removed-task',
            TaskDeleted::class
        );
        $this->app->bind(
            'mail.updated-task',
            TaskUpdated::class
        );
        $this->app->when(SendNotificationForRemovedTaskJob::class)
            ->needs(MessengerInterface::class)
            ->give(function (Application $app) {
                return $app->make(Messenger::class, ['purpose' => 'mail.removed-task']);
            });
        $this->app->when(SendTaskToEmailJob::class)
            ->needs(MessengerInterface::class)
            ->give(function (Application $app) {
                return $app->make(Messenger::class, ['purpose' => 'mail.created-task']);
            });
        $this->app->when(SendUpdatedTaskToEmailJob::class)
            ->needs(MessengerInterface::class)
            ->give(function (Application $app) {
                return $app->make(Messenger::class, ['purpose' => 'mail.updated-task']);
            });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

    }
}
