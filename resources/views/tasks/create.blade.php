@extends('layouts.main')

@section('title', 'Создать задачу')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="POST" action="{{ route('tasks.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="input-group mb-3">
            <span class="input-group-text" id="inputGroup-sizing-default">Ім'я</span>
            <input
                type="text"
                class="form-control"
                name="name"
                aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-default"
                placeholder="Введіть ім'я"
                value="{{ old('name') }}"
                class="@error('name') is-invalid @enderror"
            >
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="input-group mb-3">
            <span class="input-group-text" id="inputGroup-sizing-default">Опис</span>
            <input
                type="text"
                class="form-control"
                name="description"
                aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-default"
                placeholder="Введіть опис"
                value="{{ old('description') }}"
                class="@error('description') is-invalid @enderror"
            >
        </div>
        @error('description')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelectCategory">Категорія</label>
            <select class="form-select" name="category_id" id="inputGroupSelect01">
                <option selected>Оберіть категорію...</option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : ''}}>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelectCategory">Виконавці</label>
            <select class="form-select" multiple name="users[]" id="inputGroupSelect02">
                <option disabled>Оберіть виконавцев...</option>
                @foreach($users as $user)
                    <option value="{{ $user->id }}" {{ in_array( $user->id, old('users', []))  ? 'selected' : ''}}>{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
        @error('users')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="input-group mb-3">
            <span class="input-group-text" id="inputGroup-sizing-default">Строк</span>
            <input
                type="date"
                class="form-control"
                name="deadline"
                aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-default"
                value="{{ old('deadline') }}"
                class="@error('deadline') is-invalid @enderror"
            >
        </div>
        @error('deadline')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="image" class="form-label">Зображення</label>
            <input class="form-control" type="file" id="image" name="image">
        </div>
        @error('image')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit">Send</button>
    </form>
@endsection
